AUTOKADA CRM DOKUMENTĀCIJA
===========================

1. Home: uzdevumu statuss / statistika
-------------------------------------------

.. image:: pics/1.PNG
   :scale: 80%

2. CRM: Customers (klienti)
-------------------------------

Uzspiežot uz kāda no klientiem šajā logā, atveras klienta kopsavilkuma logs - skat. 6. Attēlu

.. image:: pics/2.PNG
   :scale: 80%

3. CRM: Debt (klientu parādi)
-------------------------------------------------

.. image:: pics/3.PNG
   :scale: 80%

4. Fleet: Klientu auto saraksts
----------------------------------

.. image:: pics/4.PNG
   :scale: 80%

5. Klienta kartiņa
--------------------------------------

.. image:: pics/6.PNG
   :scale: 80%

5.1. Komunikācija ar klientu
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: pics/8.PNG
   :scale: 80%

5.2. Atgādinājuma izveide
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: pics/9.PNG
   :scale: 40%

5.3. Piezīme / Komentārs izveide
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: pics/10.PNG
   :scale: 40%

5.4. Aktivitāšu kopsavilkums
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: pics/11.PNG
   :scale: 80%

5.5. Kontakti
^^^^^^^^^^^^^

.. image:: pics/12.PNG
   :scale: 80%

5.6. Aptauja
^^^^^^^^^^^^^^^^^^^

.. image:: pics/13.PNG
   :scale: 40%

5.7. Dokumenti
^^^^^^^^^^^^^^

.. image:: pics/14.PNG
   :scale: 40%

5.8. Izveidot / labot klienta kontaktpersonu
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: pics/15.PNG
   :scale: 40%

6. Klienta parāds
-------------------------------------------

.. image:: pics/7.PNG
   :scale: 80%

7. Klientu iedalījums
-------------------------------------------

.. image:: pics/16.PNG
   :scale: 80%

